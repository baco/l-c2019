Lab 2019
========

Week 01
-------
**Lab1**: Allí se encuentra la definición de la gramática abstracta para un
lenguaje simple de expresiones aritméticas y booleanas. La tarea consiste en
definir la función sem que define la semántica para cada una de las
construcciones sintácticas.

Week 02
-------
**Lab2**: La tarea consiste en extender el lenguaje implementado en el Lab1 con
variables enteras.

Week 03
-------
Implementar la semántica denotacional para el lenguaje imperativo
extremadamente simple (LIES) extendiendo el lenguaje simple de expresiones
(implementado en el laboratorio 2) con asignación de variables enteras,
secuencia y expresión condicional.

Week 05
-------
**Lab4**: Implementar la semántica denotacional para el lenguaje imperativo
simple con fallas (LIS + Fallas) extendiendo el lenguaje imperativo
extremadamente simple (LIES).

Week 11
-------
**Lab5**: Implementar la semántica denotacional para el lenguaje imperativo
simple con fallas, input y output (LIS + Fallas + IO) extendiendo el lenguaje
imperativo simple con fallas (LIS + fallas).