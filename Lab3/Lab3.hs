{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}

data Expr a
    = Const Int
    | Var Iden
    | Op (Expr Int)
    | Mas (Expr Int) (Expr Int)
    | Prod (Expr Int) (Expr Int)
    | Div (Expr Int) (Expr Int)
    | AConst Bool
    | Equiv (Expr Int) (Expr Int)
    | Menor (Expr Int) (Expr Int)
    | Mayor (Expr Int) (Expr Int)
    | MenorEq (Expr Int) (Expr Int)
    | MayorEq (Expr Int) (Expr Int)
    | Not (Expr Bool)
    | Conj (Expr Bool) (Expr Bool)
    | Assign Iden (Expr Int)
    | If (Expr Bool) (Expr State) (Expr State)
    | Seq (Expr State) (Expr State)
    deriving (Show)

class DomSem dom where
    sem :: Expr dom -> State -> dom

instance DomSem Int where
    sem (Const n) σ = n
    sem (Var v) σ = σ v
    sem (Op e) σ = -(sem e σ)
    sem (Mas e e') σ = sem e σ + sem e' σ
    sem (Prod e e') σ = sem e σ * sem e' σ
    sem (Div e e') σ = sem e σ `div` sem e' σ

instance DomSem Bool where
    sem (AConst b) σ = b
    sem (Equiv e e') σ = sem e σ == sem e' σ
    sem (Menor e e') σ = sem e σ < sem e' σ
    sem (Mayor e e') σ = sem e σ > sem e' σ
    sem (MenorEq e e') σ = sem e σ <= sem e' σ
    sem (MayorEq e e') σ = sem e σ >= sem e' σ
    sem (Not b) σ = not (sem b σ)

instance DomSem State where
    sem (Assign v e) σ = update σ v (sem e σ)
    sem (If b c c') σ =
        if sem b σ
            then sem c σ
            else sem c' σ
    sem (Seq c c') σ = sem c' (sem c σ)

type Iden = String
type State = Iden -> Int

update :: State -> Iden -> Int -> State
update σ v n v' = if v == v' then n else σ v'

instance DomSem (State -> Int) where
    sem x = undefined

instance DomSem (State -> Bool) where
    sem x = undefined

σ :: State
σ "x" = 5
σ "y" = 3
σ _ = 0


{- | Examples:
@
*Main> sem (Const 42) σ :: Int
42
*Main> sem (Op (Const 42)) σ :: Int
-42
*Main> sem (Mas (Const 5) (Const 3)) σ :: Int
8
*Main> sem (Prod (Const 5) (Const 3)) σ :: Int
15
*Main> sem (Div (Const 6) (Const 3)) σ :: Int
2
*Main> sem (Var "x") σ :: Int
5
*Main> sem (Var "y") σ :: Int
3
*Main> sem (Var "z") σ :: Int
0
*Main> sem (Mas (Const 37) (Var "x")) σ :: Int
42

*Main> sem (AConst True) σ :: Bool
True
*Main> sem (Equiv (Const 5) (Const 3)) σ :: Bool
False
*Main> sem (Menor (Const 5) (Const 3)) σ :: Bool
False
*Main> sem (Mayor (Const 5) (Const 3)) σ :: Bool
True
*Main> sem (MenorEq (Const 4) (Const 4)) σ :: Bool
True
*Main> sem (MenorEq (Const 5) (Const 3)) σ :: Bool
False
*Main> sem (MayorEq (Const 4) (Const 4)) σ :: Bool
True
*Main> sem (MayorEq (Const 5) (Const 3)) σ :: Bool
True
*Main> sem (Not (Equiv (Const 5) (Const 3))) σ :: Bool
True

*Main> sem (Assign "v" (Const 42)) σ "v" :: Int
42
*Main> sem (Seq (Assign "v" (Const 37)) (Assign "v" (Mas (Var "v") (Var "x")))) σ "v" :: Int
42
*Main> sem (If (AConst True) (Assign "v" (Mas (Var "v") (Var "x"))) (Assign "v" (Op (Var "x")))) σ "v" :: Int
5
*Main> sem (If (AConst False) (Assign "v" (Mas (Var "v") (Var "x"))) (Assign "v" (Op (Var "x")))) σ "v" :: Int
-5
@
-}
