{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}

data Expr a
    = Const Int
    | Op (Expr Int)
    | Mas (Expr Int) (Expr Int)
    | Prod (Expr Int) (Expr Int)
    | Div (Expr Int) (Expr Int)
    | AConst Bool
    | Equiv (Expr Int) (Expr Int)
    | Menor (Expr Int) (Expr Int)
    | Mayor (Expr Int) (Expr Int)
    | MenorEq (Expr Int) (Expr Int)
    | MayorEq (Expr Int) (Expr Int)
    | Not (Expr Bool)
    | Conj (Expr Bool) (Expr Bool)
    deriving (Show)

class DomSem dom where
    sem :: Expr dom -> dom

instance DomSem Int where
    sem (Const n) = n
    sem (Op e) = -(sem e)
    sem (Mas e e') = sem e + sem e'
    sem (Prod e e') = sem e * sem e'
    sem (Div e e') = sem e `div` sem e'

instance DomSem Bool where
    sem (AConst b) = b
    sem (Equiv e e') = sem e == sem e'
    sem (Menor e e') = sem e < sem e'
    sem (Mayor e e') = sem e > sem e'
    sem (MenorEq e e') = sem e <= sem e'
    sem (MayorEq e e') = sem e >= sem e'
    sem (Not b) = not (sem b)


{- | Examples:
@
*Main> sem (Const 42) :: Int
42
*Main> sem (Op (Const 42)) :: Int
-42
*Main> sem (Mas (Const 5) (Const 3)) :: Int
8
*Main> sem (Prod (Const 5) (Const 3)) :: Int
15
*Main> sem (Div (Const 6) (Const 3)) :: Int
2

*Main> sem (Equiv (Const 5) (Const 3)) :: Bool
False
*Main> sem (Menor (Const 5) (Const 3)) :: Bool
False
*Main> sem (Mayor (Const 5) (Const 3)) :: Bool
True
*Main> sem (MenorEq (Const 4) (Const 4)) :: Bool
True
*Main> sem (MenorEq (Const 5) (Const 3)) :: Bool
False
*Main> sem (MayorEq (Const 4) (Const 4)) :: Bool
True
*Main> sem (MayorEq (Const 5) (Const 3)) :: Bool
True
*Main> sem (Not (Equiv (Const 5) (Const 3))) :: Bool
True
@
-}
