{-# LANGUAGE GADTs #-}

--         ∞
-- fix f = ⊔ fⁱ ⊥
--        i=0
fix :: (a -> a) -> a
fix f = let x = f x in x

type Iden = String
type Σ = Iden -> Int

data Σ' = Normal Σ | Abort Σ

update :: Σ -> Iden -> Int -> Σ
update σ v n v' = if v == v' then n else σ v'

data Expr a where
    Skip :: Expr Σ'
    Const :: Int -> Expr Int
    Var :: Iden -> Expr Int
    Eq :: Expr Int -> Expr Int -> Expr Bool
    Plus :: Expr Int -> Expr Int -> Expr Int
    BConst :: Bool -> Expr Bool
    Assign :: String -> Expr Int -> Expr Σ'
    If :: Expr Bool -> Expr Σ' -> Expr Σ' -> Expr Σ'
    Seq :: Expr Σ' -> Expr Σ' -> Expr Σ'
    While :: Expr Bool -> Expr Σ' -> Expr Σ'
    NewVar :: Iden -> Expr Int -> Expr Σ' -> Expr Σ'
    Fail :: Expr Σ'
    Catch :: Expr Σ' -> Expr Σ' -> Expr Σ'

class DomSem dom where
    sem :: Expr dom -> Σ -> dom

instance DomSem Int where
    sem (Const a) = \_ -> a
    sem (Var iden) = \σ -> σ iden
    sem (Plus e1 e2) = \σ -> (+) (sem e1 σ) (sem e2 σ)

instance DomSem Bool where
    sem (BConst b) = \_ -> b
    sem (Eq e e') = \σ -> sem e σ == sem e' σ

(*.) :: (Σ -> Σ') -> Σ' -> Σ'
(*.) f (Normal σ) = f σ
(*.) f (Abort σ) = Abort σ

(†.) :: (Σ -> Σ) -> Σ' -> Σ'
(†.) f (Abort σ) = Abort (f σ)
(†.) f (Normal σ) = Normal (f σ)

(+.) :: (Σ -> Σ') -> Σ' -> Σ'
(+.) f (Abort σ) = f σ
(+.) f (Normal σ) = Normal σ

instance DomSem Σ' where
    sem Skip = \σ -> Normal σ
    sem Fail = \σ -> Abort σ
    sem (Assign v n) = \σ -> Normal (update σ v (sem n σ))
    sem (If b p p') = \σ ->
        if sem b σ
            then sem p σ
            else sem p' σ
    sem (Seq c c') = \σ -> sem c' *. (sem c σ)
    sem (Catch c c') = \σ -> sem c' +. (sem c σ)
    sem (NewVar v e c) = \σ ->
        (\σ' -> update σ' v (σ v))
            †. (sem c (update σ v (sem e σ)))
    sem (While b p) = fix f
      where
        f :: (Σ -> Σ') -> (Σ -> Σ')
        f w σ
            | sem b σ = w *. (sem p σ)
            | otherwise = Normal σ
