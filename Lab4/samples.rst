Samples:
========

Set initial state σ for samples:

.. code-block:: haskell

   *Main> :{
   *Main| σ :: Σ
   *Main| σ "x" = 5
   *Main| σ "y" = 3
   *Main| σ _ = 0
   *Main| :}


Running the samples in GHCi
---------------------------
.. code-block:: haskell

  *Main> sem (Const 42) σ
  42
  *Main> sem (Var "x") σ
  5
  *Main> sem (Var "y") σ
  3
  *Main> sem (Var "z") σ
  0
  *Main> sem (Plus (Const 5) (Const 3)) σ
  8
  *Main> sem (Plus (Const 37) (Var "x")) σ
  42
  
  *Main> sem (Eq (Const 5) (Const 3)) σ
  False
  *Main> let Normal σ' = sem Skip σ
  *Main> σ' "x"
  5
  *Main> sem (BConst True) σ
  True
  
  *Main> let Normal σ' = sem (Assign "v" (Const 42)) σ
  *Main> σ' "v"
  42
  *Main> let Normal σ' = sem (If (Eq (Var "x") (Const 5)) (Assign "x" (Const 42)) Skip) σ
  *Main> σ' "x"
  42
  *Main> let Normal σ' = sem (If (Eq (Var "x") (Const 8)) (Assign "x" (Const 42)) Skip) σ
  *Main> σ' "x"
  5
  *Main> let Normal σ' = sem (Seq (Assign "v" (Const 37)) (Assign "v" (Plus (Var "v") (Var "x")))) σ
  *Main> σ' "v"
  42
  *Main> let Abort σ' = sem Fail σ
  *Main> σ' "x"
  5
  *Main> let Normal σ' = sem (While (BConst False) (Assign "x" (Plus (Var "x") (Const 1)))) σ
  *Main> σ' "x"
  5
  *Main> let Normal σ' = sem (While (Eq (Var "x") (Const 5)) (Assign "x" (Plus (Var "x") (Const 1)))) σ
  *Main> σ' "x"
  6
  *Main> let Normal σ' = sem (While (BConst True) Skip) σ
  *Main> σ' "x"
  ^CInterrupted.
  *Main> let Normal σ' = sem (NewVar "x" (Const 0) (If (Eq (Var "x") (Const 0)) (Assign "y" (Const 8)) Skip)) σ
  *Main> (σ "x", σ "y", σ' "x", σ' "y")
  (5,3,5,8)
  *Main> let Normal σ' = sem (Catch Skip (Assign "x" (Const 42))) σ
  *Main> σ' "x"
  5
  *Main> let Normal σ' = sem (Catch Fail (Assign "x" (Const 42))) σ
  *Main> σ' "x"
  42


.. vim: ft=rst
