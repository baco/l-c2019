Recapitulación de algunas pruebas y resúmenes de clases
=======================================================

Disclaimers
-----------

* Iswim-cheatsheet está hecho en hoja A3 por los tamaños de las tipografías
  para LaTeX, se puede imprimir perfectamente escalando la hoja a A4.

* Algunos TeX están a la mitad de un proceso de optimización. Como de usar
  comandos personalizados para no repetir las mismas estructras montón de
  veces, o usar los paquetes de LaTeX correctos para cada caso.  Es algo que
  pienso ir acomodando cuando tenga el tiempo.

* La primer versión de los documentos la arranqué en un
  `OverLeaf <https://www.overleaf.com/read/ndjxccdyghmk>`_ pero se complicaba
  la recepción de aportes y el versionado (acá me pueden hacer un MR para las
  sugerencias).
