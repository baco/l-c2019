# Laboratorio Lenguajes y Compiladores #

## Enunciado ##

Extender el lenguaje imperativo simple + fallas con input-output.

**Tarea**
Extender la sintaxis abstracta con los constructores correspondientes y
re-definir la función semántica

                           sem :: Expr dom → Σ → dom

que implementa el significado de estas expresiones; implementar las instancias
de la clase DomSem para Int, Bool y Ω.

0.5 - Combinar el lab4 con las nuevas definiciones de dominio (Ω), la clase Eval
      y sus instancias contenidas en Lab5.hs
1   - Agregar los nuevos constructores para input y output utilizando GADTs
2   - Implementar la función semántica para las nuevas construcciones.

## Ejemplos ##

Asumiendo algunos nombres de constructores, particularmente EMark (!) y
QMark (?).

**Ejemplo 1**
while x < 10 do
  !x ;
  x := x + 1
od

ej1 :: Expr Ω
ej1 = While (Lt (Var "x") (Const 10)) $
            Seq (EMark $ Var "x")
                (Assign "x" (Plus (Var "x") (Const 1)))

eval_ej1 :: IO ()
eval_ej1 = eval ej1 (\_ -> 0)

**Ejemplo 2**
while y < 10 do
  ?x ;
  !x ;
  !y ;
  y := y + 1
od

ej2 :: Expr Ω
ej2 = While (Lt (Var "y") (Const 10)) $
            Seq (Seq (Seq (QMark "x")
                          (EMark $ Var "x")
                     )
                     (EMark $ Var "y")
                )
                (Assign "y" (Plus (Var "y") (Const 1)))

eval_ej2 :: IO ()
eval_ej2 = eval ej2 (\_ -> 0)

**Ejemplo 3**
?x ;
newvar x := 10 in !x end ;
!x

ej3 :: Expr Ω
ej3 = Seq (Seq (QMark "x")
               (Newvar "x" (Const 10)
                       (EMark $ Var "x")
               )
          )
          (EMark $ Var "x")

eval_ej3 :: IO ()
eval_ej3 = eval ej3 (\_ -> 0)

## Uso ##

```console
$ ghci Lab5.hs
*Main> eval_ej1
0
1
2
3
4
5
6
7
8
9
*Main>
```
