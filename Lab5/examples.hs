import Lab5

-- | Examples

{- * Example 1
@
while x < 10 do
  !x ;
  x := x + 1
od
@
-}
eg1 :: Expr Ω
eg1 =
    While (Lt (Var "x") (Const 10)) $
        Seq
            (EMark $ Var "x")
            (Assign "x" (Plus (Var "x") (Const 1)))
eval_eg1 :: IO ()
eval_eg1 = eval eg1 (\_ -> 0)

{- * Example 2
@
while y < 10 do
  ?x ;
  !x ;
  !y ;
  y := y + 1
od
@
-}
eg2 :: Expr Ω
eg2 =
    While (Lt (Var "y") (Const 10)) $
        Seq
            ( Seq
                ( Seq
                    (QMark "x")
                    (EMark $ Var "x")
                )
                (EMark $ Var "y")
            )
            (Assign "y" (Plus (Var "y") (Const 1)))
eval_eg2 :: IO ()
eval_eg2 = eval eg2 (\_ -> 0)

{- * Example 3
@
?x ;
newvar x := 10 in !x end ;
!x
@
-}
eg3 :: Expr Ω
eg3 =
    Seq
        ( Seq
            (QMark "x")
            ( NewVar
                "x"
                (Const 10)
                (EMark $ Var "x")
            )
        )
        (EMark $ Var "x")
eval_eg3 :: IO ()
eval_eg3 = eval eg3 (\_ -> 0)

{- | Usage
@
```console
\$ ghci Lab5.hs
```
@
@
*Main> eval_eg1
0
1
2
3
4
5
6
7
8
9
*Main> 
@
-}


-- > !x
eg4 :: Expr Ω
eg4 = EMark (Var "x")
-- > *Main> eval eg4 (\_ -> 42)

{- @
?x
if x < 10 then
    !(y + 1)
else
    !(x + x)
fi
@ -}
eg5 :: Expr Ω
eg5 =
    Seq
        (QMark "x")
        ( If
            (Lt (Var "x") (Const 10))
            (EMark (Plus (Var "y") (Const 8)))
            (EMark (Plus (Var "x") (Var "x")))
        )
-- > *Main> eval eg5 (\_ -> 7)

{- @
catchin
    fail
with
    ?x
    y := y + x
!(y + 8)
@ -}
eg6 :: Expr Ω
eg6 =
    Seq
        ( Catch
            Fail
            ( Seq
                (QMark "x")
                (Assign "y" (Plus (Var "y") (Var "x")))
            )
        )
        (EMark (Plus (Var "y") (Const 8)))
-- > *Main> eval eg6 (\_ -> 7)

{- @
catchin
    ?x
    if 8 < x then
        fail
    else
        y := 13
with
    y := y + x
!(y + 8)
@ -}
eg7 :: Expr Ω
eg7 =
    Seq
        ( Catch
            ( Seq
                (QMark "x")
                ( If
                    (Lt (Const 8) (Var "x"))
                    Fail
                    (Assign "y" (Const 13))
                )
            )
            (Assign "y" (Plus (Var "y") (Var "x")))
        )
        (EMark (Plus (Var "y") (Const 8)))
-- > *Main> eval eg7 (\_ -> 7)
